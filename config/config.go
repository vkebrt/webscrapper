package config

import()


/* Config - object with complete configuration for app, this module is used to setup whole app,
 * from reading from config file, cross reading from env file and so on
 */
type Config struct {
	SourcesList []dataSource
	ParsersList []dataParser
	AnalyticsList []dataAnalyzer
	Threaded bool
	MaxThreads uint
	DbType string
	DbURI string
	IndexIdentifier string
	IndexType string
}

type dataSource struct {
}

type dataParser struct {
}

type dataAnalyzer struct {
}


